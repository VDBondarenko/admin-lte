<h2>Как развернуть проект локально.</h2>
<ol>
	<li>git clone https://VDBondarenko@bitbucket.org/VDBondarenko/admin-lte.git</li>
	<li>composer install</li>
	<li>В .env фалйле заполнить переменную DATABASE_URL(DATABASE_URL=mysql://root:1234@db:3306/eltex?serverVersion=10.4).</li>
	<li>Выполнить команду php bic\console doctrine:database:create если базы данных нет.</li>
	<li>php bic\console doctrine:migrations:migrate</li>
	<li>Выполнить команду php bic\console c:c.</li>
</ol>

<h2>Как развернуть проект на Docker.</h2>
<ol>
	<li>git clone https://VDBondarenko@bitbucket.org/VDBondarenko/admin-lte.git</li>
	<li>Выполнить команду docker-compose --build</li>
	<li>В .env (NGINX_EXPOSED_PORT) фалйле указать желаемы порт на которм будет работать проект. По умолчанию 80.</li>
	<li>Запустить контейнеры docker-compose up -d </li>
	<li>Зайти в контейнеры docker-compose exec web bash</li>
	<li>Выполнить команду composer install</li>
	<li>Выполнить команду php bic\console doctrine:database:create.</li>
	<li>php bic\console doctrine:migrations:migrate</li>
	<li>Выполнить команду php bic\console c:c.</li>
</ol>  

<h4>Логин и пароль админа.</h4> 
<p>
    логин: admin@eltex.ua<br>
    пароль: 1
</p>

<h4>Rest users</h4>
<p>
 url: api/user<br>
 Для авторизации нужно передать X-AUTH-TOKEN в заголовке.<br> 
 К API доступ только у администраторов. <br>
 Token пользователя admin@eltex.ua: bhoCcVAYPIh-6OnvXT0qmnUmjBvFdEnskmDvk8q-jEc
</p>
