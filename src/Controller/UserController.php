<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * @var TokenGeneratorInterface
     */
    protected $tokenGenerator;

    /**
     * @var Security
     */
    private $security;

    /**
     * @var SluggerInterface
     */
    private $slugger;

    public function __construct(UserPasswordEncoderInterface $encoder, TokenGeneratorInterface $tokenGenerator, Security $security, SluggerInterface $slugger)
    {
        $this->encoder = $encoder;
        $this->tokenGenerator = $tokenGenerator;
        $this->security = $security;
        $this->slugger = $slugger;
    }

    /**
     * @Route("/", name="user_index", methods={"GET"})
     * @param EntityManagerInterface $em
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    public function index(EntityManagerInterface $em, PaginatorInterface $paginator, Request $request): Response
    {

        $pagination = $paginator->paginate(
            $em->getRepository(User::class)->findAllQuery()->getQuery(),
            $request->query->getInt('page', 1),
            5
        );

        return $this->render(
            'user/index.html.twig',
            [
                'pagination' => $pagination
            ]
        );
    }

    /**
     * @Route("/new", name="user_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $currentUser = $this->security->getUser();
        $user = new User();
        $form = $this->createForm(UserType::class, $user, ['user' => $currentUser] );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->saveAvatar($form, $user);
            $this->encodePassword($form, $user);

            $user->setApiToken($this->tokenGenerator->generateToken());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render(
            'user/new.html.twig',
            [
                'user' => $user,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/{id}/show", name="user_show", methods={"GET"})
     * @param User $user
     * @return Response
     */
    public function show(User $user): Response
    {
        return $this->render(
            'user/show.html.twig',
            [
                'user' => $user,
            ]
        );
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function edit(Request $request, User $user): Response
    {

        $currentUser = $this->security->getUser();
        $form = $this->createForm(UserType::class, $user, ['user' => $currentUser]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $this->saveAvatar($form, $user);
            $this->encodePassword($form, $user);

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render(
            'user/edit.html.twig',
            [
                'user' => $user,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/{id}/delete", name="user_delete", methods={"DELETE"})
     * @param Request $request
     * @param User $user
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function delete(Request $request, User $user, EntityManagerInterface $em): Response
    {
        $admins = $em->getRepository(User::class)
            ->checkAdminStatus($user);
        if (!count($admins)){
            $this->addFlash('error','cannot delete the last system administrator');
            return $this->redirectToRoute('user_index');
        }

        if ($this->isCsrfTokenValid('delete' . $user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_index');
    }

    /**
     * @param FormInterface $form
     * @param User $user
     */
    protected function saveAvatar(FormInterface $form, User $user ):void
    {
        if ($form->has('avatar')) {
            $avatar = $form->get('avatar')->getData();
            if ($avatar) {
                $originalFilename = pathinfo($avatar->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = $this->slugger->slug($originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.' . $avatar->guessExtension();
                $avatar->move(
                    $this->getParameter('avatar_directory'),
                    $newFilename
                );

                $user->setAvatar($newFilename);
            }
        }
    }

    protected function encodePassword(FormInterface $form, User $user ):void
    {
        if ($form->has('password')) {
            $password = $form->get('password')->getData();
            if (!empty($password)) {
                $password = $this->encoder->encodePassword($user, $password);
                $user->setPassword($password);
            }
        }
    }
}
