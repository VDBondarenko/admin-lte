<?php


namespace App\Controller\API;

use App\Entity\User;
use App\Form\API\UserAPIType;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use JMS\Serializer\SerializerInterface;

/**
 * Class UserAPIController
 * @package App\Controller\API
 * @Route("/api/user")
 */
class UserAPIController extends AbstractAPIController
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(EntityManagerInterface $em, SerializerInterface $serializer, UserPasswordEncoderInterface $encoder)
    {
        parent::__construct($em, $serializer);
        $this->encoder = $encoder;
    }

    /**
     * @Route("/{id}", name="user_get_api", methods={"GET"})
     * @param $id
     * @return Response
     */
    public function showAction($id)
    {
        $user = $this->em->getRepository(User::class)->find($id);

        if (!$user) {
            throw $this->createNotFoundException(sprintf('No user found with id "%s"', $id));
        }

        return $this->createApiResponse($this->serialize($user), 200);
    }

    /**
     * @Route("/",  name="user_list_api", methods={"GET"})
     */
    public function listAction()
    {
        $users = $this->getDoctrine()
            ->getRepository(User::class)
            ->findAll();

        return $this->createApiResponse($this->serialize($users), 200);
    }


    /**
     * @Route("/", name="uset_post_api", methods={"POST"} )
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function newAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $user = new User();

        $data = $this->encodePassword($data, $user);
        $form = $this->createForm(UserAPIType::class, $user);
        $form->submit($data);

        if (!$form->isValid()) {
            return $this->createValidationErrorResponse($form);
        }


        $this->em->persist($user);
        $this->em->flush();

        return $this->createApiResponse($this->serialize($user), 201, [
            'Location' => $this->generateUrl('user_get_api', ['id' => $user->getId()])
        ] );
    }

    /**
     * @Route("/{id}",  name="uset_put_api", methods={"PUT"})
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function updateAction($id, Request $request)
    {
        $user = $this->em->getRepository(User::class)->find($id);

        if (!$user) {
            throw $this->createNotFoundException(sprintf('No user found with id "%s"', $user));
        }

        $data = json_decode($request->getContent(), true);
        $data = $this->encodePassword($data, $user);
        $form = $this->createForm(UserAPIType::class, $user);
        $form->submit($data);
        $this->em->persist($user);
        $this->em->flush();

        return $this->createApiResponse($this->serialize($user), 200);
    }

    /**
     * @Route("/{id}",  name="uset_delete_api", methods={"DELETE"})
     * @param $id
     * @return Response
     */
    public function deleteAction($id)
    {
        $user = $this->em->getRepository(User::class)->find($id);

        if (!$user) {
            throw $this->createNotFoundException(sprintf('No user found with id "%s"', $user));
        }

        $this->em->remove($user);
        $this->em->flush();

        return $this->createApiResponse(null, 204);
    }

    /**
     * @param array $data
     * @param User $user
     * @return array
     */
    protected function encodePassword(array $data, User $user):array
    {
        if (key_exists('password', $data)){
            $data['password'] = $this->encoder->encodePassword($user, $data['password']);
        }

        return $data;
    }

}