<?php


namespace App\Controller\API;


use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\FormInterface;

/**
 * Class AbstractAPIController
 * @package App\Controller\API
 */
class AbstractAPIController extends AbstractController
{

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * AbstractAPIController constructor.
     * @param EntityManagerInterface $em
     * @param SerializerInterface $serializer
     */
    public function __construct( EntityManagerInterface $em, SerializerInterface $serializer)
    {
        $this->em = $em;
        $this->serializer = $serializer;
    }

    /**
     * @param $data
     * @param int $statusCode
     * @param array $headers
     * @return Response
     */
    protected function createApiResponse($data, $statusCode = 200, array $headers = [])
    {
        $headers['Content-Type'] = 'application/json';
        $json = $this->serialize($data);
        return new Response($json, $statusCode, $headers);
    }

    /**
     * @param FormInterface $form
     * @return JsonResponse
     */
    protected function createValidationErrorResponse(FormInterface $form)
    {
        $errors = $this->getErrorsFromForm($form);
        $data = [
            'type' => 'validation_error',
            'title' => 'There was a validation error',
            'errors' => $errors
        ];
        $response = new JsonResponse($data, 400);
        $response->headers->set('Content-Type', 'application/problem+json');
        return $response;
    }

    /**
     * @param $data
     * @param string $format
     * @return string
     */
    protected function serialize($data, string $format = 'json'):string
    {
        $context = new SerializationContext();
        $context->setSerializeNull(true);

        return $this->serializer->serialize(
            $data,
            $format,
            $context
        );
    }

    protected function getErrorsFromForm(FormInterface $form)
    {
        $errors = array();
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }
        return $errors;
    }
}