<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    /**
     * @param User $user
     * @return int|mixed|string
     */
    public function checkAdminStatus(User $user)
    {
        $sql = 'select *
            from user u 
            where JSON_CONTAINS(roles, \'["ROLE_ADMIN"]\') and user_status_id = 1 and id != :id;';

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata(User::class, 'u');

        return $this->_em->createNativeQuery($sql, $rsm)
            ->setParameter('id', $user->getId())
            ->getResult();
    }

    /**
     * @param User $user
     * @return int|mixed|string
     */
    public function checkAdminRole(User $user)
    {
        $sql = 'select *
            from user u 
            where JSON_CONTAINS(roles, \'["ROLE_ADMIN"]\') and id != :id;';

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata(User::class, 'u');

        return $this->_em->createNativeQuery($sql, $rsm)
            ->setParameter('id', $user->getId())
            ->getResult();
    }

    public function findAllQuery(){
        return $this->createQueryBuilder('q');
    }

}
