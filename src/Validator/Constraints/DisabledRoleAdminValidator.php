<?php


namespace App\Validator\Constraints;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Class DisabledRoleAdminValidator
 * @package App\Validator\Constraints
 */
class DisabledRoleAdminValidator extends ConstraintValidator
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * DisabledRoleAdminValidator constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param mixed $entity
     * @param Constraint $constraint
     */
    public function validate($entity, Constraint $constraint)
    {
        if (!$constraint instanceof DisabledRoleAdmin) {
            throw new UnexpectedTypeException($constraint, DisabledRoleAdmin::class);
        }

        if(!empty($entity->getId()) && !in_array('ROLE_ADMIN', $entity->getRoles())){
            $admins = $this->em->getRepository(User::class)
                ->checkAdminRole($entity);


            if (!count($admins)){
                $this->context->buildViolation($constraint->message)
                    ->addViolation();
            }
        }
    }
}