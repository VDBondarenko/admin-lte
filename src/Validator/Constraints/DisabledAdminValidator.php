<?php


namespace App\Validator\Constraints;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class DisabledAdminValidator extends ConstraintValidator
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function validate($entity, Constraint $constraint)
    {
        if (!$constraint instanceof DisabledAdmin) {
            throw new UnexpectedTypeException($constraint, DisabledAdmin::class);
        }

        if(!empty($entity->getId()) && $entity->getUserStatus()->getId() !== 1){
            $admins = $this->em->getRepository(User::class)
                ->checkAdminStatus($entity);

            if (!count($admins)){
                $this->context->buildViolation($constraint->message)
                    ->addViolation();
            }
        }
    }
}