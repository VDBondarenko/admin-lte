<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\UserStatus;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class UserType extends AbstractType
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(
        Security $security
    ) {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        if ($this->security->isGranted('ROLE_MODERATOR', $options['user'])) {
            $builder->add(
                'userStatus',
                EntityType::class,
                [
                    'class' => UserStatus::class,
                ]
            );
        }

        if ($this->security->isGranted('ROLE_ADMIN', $options['user']) ||
            $this->security->isGranted('ROLE_CREATOR', $options['user'])
        ) {
            $builder
                ->add('email')
                ->add('firstName')
                ->add('lastName')
                ->add('birthday', BirthdayType::class)
                ->add(
                    'note',
                    TextareaType::class,
                    [
                        'required' => false
                    ]
                )
                ->add(
                    'roles',
                    ChoiceType::class,
                    [
                        'multiple' => true,
                        'choices' => [
                            'Administrator' => 'ROLE_ADMIN',
                            'Moderator' => 'ROLE_MODERATOR',
                            'Creator' => 'ROLE_CREATOR'
                        ]
                    ]
                )
                ->add(
                    'password',
                    PasswordType::class,
                    [
                        'required' => $options['data']->getId()? false : true,
                        'mapped'=> $options['data']->getId()? false : true,
                    ]
                )
                ->add(
                    'userStatus',
                    EntityType::class,
                    [
                        'class' => UserStatus::class,
                    ]
                )
                ->add(
                    'avatar',
                    FileType::class,
                    [
                        'mapped' => false,
                        'required' => false
                    ]
                );
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => User::class,
            ]
        );
        $resolver->setRequired('user');
    }
}
