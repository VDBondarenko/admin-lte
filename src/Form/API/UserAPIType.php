<?php

namespace App\Form\API;

use App\Entity\User;
use App\Entity\UserStatus;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class UserAPIType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('firstName')
            ->add('lastName')
            ->add('birthday', DateType::class, [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ])
            ->add('note')
            ->add('roles', ChoiceType::class, [
                'multiple' => true,
                'choices' => [
                    'administrator' => 'ROLE_ADMIN',
                    'moderator' => 'ROLE_MODERATOR',
                    'creator' => 'ROLE_CREATOR'
                ]
            ])
            ->add('password', PasswordType::class)
            ->add('userStatus', EntityType::class,[
                'class' => UserStatus::class,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'csrf_protection' => false,
        ]);
    }
}
