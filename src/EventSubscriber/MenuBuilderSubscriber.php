<?php


namespace App\EventSubscriber;

use KevinPapst\AdminLTEBundle\Event\SidebarMenuEvent;
use KevinPapst\AdminLTEBundle\Model\MenuItemModel;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class MenuBuilderSubscriber
 * @package App\EventSubscriber
 */
class MenuBuilderSubscriber implements EventSubscriberInterface
{
    /**
     * @return array|array[]
     */
    public static function getSubscribedEvents(): array
    {
        return [
            SidebarMenuEvent::class => ['onSetupMenu', 100],
        ];
    }

    /**
     * @param SidebarMenuEvent $event
     */
    public function onSetupMenu(SidebarMenuEvent $event)
    {
        $adminPanel = new MenuItemModel('adminPanel', 'Admin Panel', 'item_symfony_route', [], 'fas fa-tachometer-alt');

        $adminPanel->addChild(new MenuItemModel('user', 'Users', 'user_index', [], 'fas fa fa-users'))
            ->addChild(new MenuItemModel('user status', 'Users Status', 'user_status_index', [], 'fas fa-id-card-alt')
        );

        $event->addItem($adminPanel);

        $this->activateByRoute(
            $event->getRequest()->get('_route'),
            $event->getItems()
        );
    }

    /**
     * @param string $route
     * @param MenuItemModel[] $items
     */
    protected function activateByRoute($route, $items)
    {
        foreach ($items as $item) {
            if ($item->hasChildren()) {
                $this->activateByRoute($route, $item->getChildren());
            } elseif ($item->getRoute() == $route) {
                $item->setIsActive(true);
            }
        }
    }
}