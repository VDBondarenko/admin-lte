<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200614223109 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user ADD last_login DATETIME DEFAULT NULL, CHANGE user_status_id user_status_id INT DEFAULT NULL, CHANGE note note VARCHAR(1024) DEFAULT NULL, CHANGE roles roles JSON NOT NULL, CHANGE api_token api_token VARCHAR(255) DEFAULT NULL');
        $this->addSql('INSERT INTO user_status (id, name) VALUES (1, \'Enabled\'); INSERT INTO user_status (id, name) VALUES (2, \'Disabled\'); INSERT INTO user_status (id, name) VALUES (3, \'Banned\');');
        $this->addSql('INSERT INTO user (id, user_status_id, email, first_name, last_name, birthday, note, roles, password, api_token, last_login) VALUES (1, 1, \'admin@eltex.ua\', \'admin\', \'admin\', \'2019-03-05\', \'admin\', \'["ROLE_ADMIN"]\', \'$argon2id$v=19$m=65536,t=4,p=1$W0R/fzw6qi/GEMx79wnuKQ$G3KpdW7WSPsU2lv6KvXavJifAB1eAFnCKHLsMrlKk/U\', \'bhoCcVAYPIh-6OnvXT0qmnUmjBvFdEnskmDvk8q-jEc\', \'2020-06-15 00:07:49\');');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user DROP last_login, CHANGE user_status_id user_status_id INT DEFAULT NULL, CHANGE note note VARCHAR(1024) CHARACTER SET utf8mb4 DEFAULT \'\'\'NULL\'\'\' COLLATE `utf8mb4_unicode_ci`, CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`, CHANGE api_token api_token VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'\'\'NULL\'\'\' COLLATE `utf8mb4_unicode_ci`');
    }
}
